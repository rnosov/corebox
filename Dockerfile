# DOCKER-VERSION 0.10.0

FROM dockerfile/nodejs

RUN npm install -g gulp

ADD . /var/www
WORKDIR /var/www

ENV VIRTUAL_PORT 3000
EXPOSE 3000
