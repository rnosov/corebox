scp units/redis.service units/mongodb.service units/proxy.service units/project@.service $1:/tmp &&\
ssh $1 "
        docker kill redis || true;
        docker rm redis || true;
        docker pull dockerfile/redis ;
        docker run -d --name redis -p 6379:6379 -v /home/core/data/redis:/data dockerfile/redis;
        sudo cp /tmp/redis.service /etc/systemd/system/redis.service ;
        sudo systemctl enable /etc/systemd/system/redis.service;
        sudo systemctl start redis.service;

        docker kill mongodb || true;
        docker rm mongodb || true;
        docker pull dockerfile/mongodb ;
        docker run -d --name mongodb -p 27017:27017 -v /home/core/data/mongodb:/data/db dockerfile/mongodb;
        sudo cp /tmp/mongodb.service /etc/systemd/system/mongodb.service;
        sudo systemctl enable /etc/systemd/system/mongodb.service;
        sudo systemctl start mongodb.service;

        docker kill proxy || true;
        docker rm proxy || true;
        docker pull jwilder/nginx-proxy ;
        docker run -d --name proxy -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock -t jwilder/nginx-proxy;
        sudo cp /tmp/proxy.service /etc/systemd/system/proxy.service;
        sudo systemctl enable /etc/systemd/system/proxy.service;
        sudo systemctl start proxy.service;

        sudo cp /tmp/project@.service /etc/systemd/system/project@.service;
        sudo systemctl enable /etc/systemd/system/project@.service;
        docker pull rnosov/corebox;
        mkdir /home/core/projects||true
       "
